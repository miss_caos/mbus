//
//  ViewController.swift
//  Autobus
//
//  Created by Ruzica on 5/17/18.
//  Copyright © 2018 Ruzica. All rights reserved.
//

import UIKit
import Alamofire



class ViewController: UIViewController {
    
    /*struct Connectivity {
        static let sharedInstance = NetworkReachabilityManager()!
        static var isConnectedToInternet:Bool {
            return self.sharedInstance.isReachable
        }
    } */

        
    @IBAction func DriverLogIn(_ sender: Any) {
        
        Alamofire.request("https://reqres.in/api/users?page=2").responseJSON{ response in
            
            switch response.result {
            case .failure:
                // handle errors (including `validate` errors) here
                print("error")
                let alertController = UIAlertController(title: "Greska!", message: "Provjerite internet konekciju", preferredStyle: UIAlertControllerStyle.alert)
                alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                    print ("OK")
                    self.dismiss(animated: true, completion: nil)
                }))
                self.present(alertController, animated: true, completion: nil)
                
            case .success:
                
                // KLIKOM NA BUTTON LOG IN SE OTVARA MAPA CONTROLLER
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let controller = storyboard.instantiateViewController(withIdentifier: "Mapa") as? Mapa
            
                
                // PRENOS PODATAKA, I VEZA SA MAPA.CONTROLLEROM
                
                controller?.role = 0
                
                self.present(controller!, animated: true, completion: nil)
                
                
                
                
                print("success")
            }
        }
        
    }
  

    
    
    override func viewDidLoad() {
        //super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
    }




}
