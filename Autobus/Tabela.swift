//
//  Tabela.swift
//  Autobus
//
//  Created by Ruzica on 5/17/18.
//  Copyright © 2018 Ruzica. All rights reserved.
//

import UIKit

class Tabela: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    
    @IBOutlet weak var Table: UITableView!
    
    
   // let cellContent = ["1 - Madjir-Nova Bolnica" , "3 - Centar-Zeleni Vir" , "3b - Centar-Debeljaci" , "6 - Aut.Stanica-Laus-Saracica" , "8 - Aut.Stanica-Podgora", "9 - Cesma-Centar-Desna Novoselija" , "9b - Cesma-Medeno polje-Centar" , "9i - Incel-Centar" , "10 - Aut.Stanica-Obilicevo" , "12 - Centar-Paprikovac" , "13 - Lazarevo-Obilicevo" , "13a - Centar-Zaluzani" , "13b - Novo Groblje-Lazarevo" , "13c - Centar-Tunjice-Zaluzani" , "13p - Obilicevo-Petricevac" , "14 - Starcevica-Centar" , "14b - Starcevica-Aut.Stanica" , "17 - Obilicevo-Nova Bolnica" , "17a - Starcevica-Nova Bolnica" , "19 - Srpski Milanovac-Centar" , "20 - Paprikovac-Aut.Stanica" , "39 - Drakulic-Centar" , "39a - Drakulic-Rak.bare-Centar"]
   
    let NOVE_LINIJE = [Linije(lineid: "10" , linename: "10 - Aut.Stanica-Obilicevo", Id: 1), Linije(lineid: "14", linename: "14 - Starcevica-Centar", Id: 0)]
    
    
     override func viewDidLoad() {
        super.viewDidLoad()
        
        Table.delegate = self
        Table.dataSource = self
        Table.backgroundColor = UIColor.clear
        // Do any additional setup after loading the view.
    }
    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return NOVE_LINIJE.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell (style: UITableViewCellStyle.default, reuseIdentifier: "Cell")
        let l = NOVE_LINIJE[indexPath.row]
        cell.textLabel?.text = l.line_name
        
        
        cell.textLabel?.backgroundColor = UIColor.black
        cell.backgroundColor = UIColor.clear
        
        return cell
    }
    
    // NACIN NA KOJI KADA KLIKNEMO NA TACVNO ODREDJENI AUTOBUS U TABLE VIEW, DA NAM GA POKAZE U MAPA.SWIFT, GDJE JE PRETHODNO POTREBNO KREIRATI VARIJABLU (LINIJA)
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "Mapa") as? Mapa
        let l = NOVE_LINIJE[indexPath.row]
        controller?.linija = l.line_id!
        
        self.present(controller!, animated: true, completion: nil)
        
        
        // DA KADA KLIKNE NA ODREDJENI BROJ LINIJE, DA ISPRINTA TAJ BROJ LINIJE
        tableView.deselectRow(at: indexPath, animated: true)
        
        let row = indexPath.row
        
        print("Row: \(row)")
        
    }
    
    
    /* func numberOfSec , tions(in tableView: UITableView) -> Int {
     return 23
     } */ 
    
 
    
    
    
}
