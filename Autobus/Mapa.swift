//
//  Mapa.swift
//  Autobus
//
//  Created by Ruzica on 5/17/18.
//  Copyright © 2018 Ruzica. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import Alamofire
import SwiftyJSON

class Mapa: UIViewController, MKMapViewDelegate , CLLocationManagerDelegate {
    
    
    @IBOutlet weak var Map: MKMapView!
    

    // TREBA NAM VARIJABLA KOJA CE VEZATI I I III CONTROLLER ROLE
    
    var role : Int = 1
    var linija : String = "10" // POTREBNA ZBOG POVEZIVANJA SA TABELA.SWIFT
    let ROLE_USER = 1
    let ROLE_DRIVER = 0
    let newPin = MKPointAnnotation()  // POTREBNA ZA PIN LOKACIJE
    
    var parameters: [String: Any]?
    
    var X = CLLocationManager()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Map.delegate = self
        Map.showsUserLocation = true

        parameters = ["LINE": linija]
       // determineMyCurrentLocation(role: role)
        
        if role == ROLE_USER {
            
            // APIs CALLED
            let apiToContact = "https://dqmnd5qta1.execute-api.eu-west-1.amazonaws.com/prod/mbus"
            // This code will call the iTunes top 25 movies endpoint listed above
            Alamofire.request("https://dqmnd5qta1.execute-api.eu-west-1.amazonaws.com/prod/mbus", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil).validate().responseJSON() { response in
                switch response.result {
                case .success:
                    if let value = response.result.value {
                        let json = JSON(value)
                        
                        let jsonArray = json["LOCATIONS"]
                        
                        let location = jsonArray[0]
                        let lat = location["lat"].doubleValue
                        let lng = location["lng"].doubleValue
                        
                        locationManager(latitude:lat,longitude:lng)
                        
                    }
                case .failure(let error):
                    print(error)
                }
            }
       
    }

   
    func determineMyCurrentLocation(role: Int){
        X = CLLocationManager()
        X.delegate = self
        X.desiredAccuracy = kCLLocationAccuracyBest
        //X.requestWhenInUseAuthorization()
        X.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            X.startUpdatingLocation()
        } else {
            print("Nije odobrena lokacija!")
        }
    }
  
    
    
    
    
        func locationManager( latitude: Double, longitude: Double ) {
      
            let latDelta: CLLocationDegrees = 0.05
            let longDelta: CLLocationDegrees = 0.05
            let raspon = MKCoordinateSpan(latitudeDelta: latDelta, longitudeDelta: longDelta)
            let lokacija = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
            let region = MKCoordinateRegion(center: lokacija , span: raspon)
            
            self.Map.setRegion(region, animated: true)
        
            newPin.coordinate = lokacija
            Map.addAnnotation(newPin)
         
        }
    
    /*func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if (annotation is MKPointAnnotation) {
            print ("Not req")
            return nil
        }
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "identifier")
        if annotationView == nil {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "identifier")
            annotationView?.canShowCallout = true
        } else {
            annotationView?.annotation = annotation
        }
        
        annotationView?.image = UIImage(named: "bus.png")
        
        return (annotationView)
        
    } */
   
     
    
    
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
        print("Error \(error)")
    }
    
    
        func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //determineMyCurrentLocation()
    }

}


}
